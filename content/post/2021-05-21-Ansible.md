
---
title: Ansible
subtitle: Configuração Básica do Ansible
date: 2021-05-21
tags: ["comandos", "sintaxes", "ansible"]
comments: false
---


Instalação - arquivos de configuração - arquivos yaml
<!--more-->
```
yum install ansible
```

	estrutura base: (já é montada automaticamente no momento da instalação)
```
	[root@srveng ~]# cd /etc/ansible/
	[root@srveng ansible]# ls -ltr
	total 24
	drwxr-xr-x. 2 root root     6 Fev 20 18:33 roles
	-rw-r--r--. 1 root root  1016 Fev 20 18:33 hosts
	-rw-r--r--. 1 root root 19985 Fev 20 18:33 ansible.cfg
```

cd /roles

mkdir files tasks

	colocar no files arquivos que serão enviados para os outras máquinas
```	
[root@srveng roles]# cd  tasks
[root@srveng tasks]# touch main.yml
``` 	
	adicionar no main.yml tasks a serem realizadas
```
---

  - name: Copiando arquivo
    copy: src=/etc/ansible/roles/files/TESTECOPIADEARQUIVO dest=/opt/

  - name: Comando mkdir
    shell:
      cmd: mkdir teste
      chdir: /opt/
```
########################################
vi /etc/ansible/hosts

	inserir os hosts e parâmetros de usuário
```
[servers]

192.168.15.100
192.168.15.101

[all:vars]


ansible_user=taty

ansible_ssh_pass=taty

ansible_ssh_common_args='-o StrictHostKeyChecking=no'

ansible_become_user=root

ansible_become_pass=taty
```
########################################

	comando para executar as tasks que estão dentro do main.yml
```
ansible-playbook main.yml
```




