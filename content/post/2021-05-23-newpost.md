---
title: Zen to Done!
subtitle: Metodologia  :)
date: 2021-05-23
tags: ["gitlab", "gitlapages"]
---

ZEN TO DONE
(metodologia aula 3 do Multirão DevOps)
@Flow_Foco - André Brandão

Fases:

1- Básico
2- Refinado
3- Especialista

--> Se comprometa com o BÁSICO:

1 - Capture - Anote tudo!! 
            - Libere a Mente!!
Aplicativo no cel... to do
                     nothion

2 - Processe - Se o que vc lembrou leva até 2 minutos FAÇA AGORA - não precisa anotar!
             - Delegar? (80% = vc)
             - Deletar?
             - Arquivar?
             - Agendar/Planejar

3 - Planeje - Divida em tarefas menores
            - TMI (de 1 a 3 por dia) MIT - Most Important a - Tarefa mais Importante

4 - Execute - Aja
            - Desligue interrupções
            - Se não tiver jeito, anote e processe
            - Break após execução


REFINADO:

1 - Listas - Há listas pessoais, e-mails, trabalho, viagem

2 - Organização - Organizar as coisas e as listas
                - O que não preciso jogo fora

ESPECIALISTA

1 - Revise - Revise tudo

2 - Simplifique - (Ou diminua) - Redes Sociais
                               - Mensagens
                               - Séries
                               - Agenda

3 - Rotina - Encaixe tudo isso em dias e horários definidos e recorrentes

Livro - O poder do Hábito



