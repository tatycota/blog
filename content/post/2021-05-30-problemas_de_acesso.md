
---
title: Problemas de Acesso 
subtitle: troubleshooting básico para problemas de acesso
date: 2021-05-30
tags: ["comandos", "sintaxes"]
comments: false
---




<!--more-->

## Problema de Acesso 

--> tentar acesso via cofre de senhas
--> tentar acesso via console
--> fazer restart do sshd e sssh


```
service sshd restart
service sssd restart
```


--> Verificar se o FS /var está com 100%
--> Verificar grupos do usuário (necessário estar no grupo wheel)


```
id <user>
usermod -aG <nomedogrupo> <user>
```


--> Verificar AllowGroups no arquivo /etc/ssh/sshd_config e se não tiver, acrescentar o grupo wheel na lista de AllowGroups (é possível usar o script pelo altiris


cat /etc/ssh/sshd_config | grep -i allow


```
cp /etc/ssh/sshd_config /etc/ssh/sshd_config-$(date +"%d%m%Y-%H%M%S")
sed -i 's/^AllowGroups.*$/& wheel/' /etc/ssh/sshd_config
service sshd restart
```


--> Para usuários de serviço, rescue e root verificar expiração do usuário


```
chage -I -1 -m 0 -M 99999 -E -1 <user>
```


--> Se necessário, verificar privilégios de root e acrescentar usuário no arquivo /etc/sudoers


```
root     ALL=(ALL) NOPASSWD: ALL
user1  ALL=(ALL) NOPASSWD: ALL
user2  ALL=(ALL) NOPASSWD: ALL
```


## AIX

Comando para stop e start no AIX


```
stop-secldapclntd e start-secldapclntd
```

## smit user

--> O reset de senha padrão ocorre quando existiu 3 tentativas erradas de senha
--> Verificar se o usuário ficou bloqueado por tentativas erradas de senha



```
lsuser -f <nomedousuário> 
```


--> Verificar parâmetro "compat", se necessário adicioná-lo
--> Verificar informações no arquivo /etc/security/user

--> O comando pwdadm administra as senhas dos usuários.
O sinalizador -q permite que o usuário root ou membros do grupo de segurança consultem informações de senha. Apenas o status do atributo lastupdate e o atributo flags são exibidos. A senha criptografada permanece oculta.


```
root@server001(/)# pwdadm -q <user>
user:
lastupdate = 1558806417
```



